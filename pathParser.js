'use strict';

const path                = require('path');
const promise             = require('bluebird');

exports.create = function (config, logger) {

  const parse = input => {
    const parsed = path.parse(input);
    return promise.resolve(parsed);
  }

  return (function () {
    return {
      parse: parse
    };
  }());
};