# s3 Handler

## Overview

* Triggered when a file is uploaded to s3 bucket
* Fetches the s3 object meta-data
* Parses object key/path
* Creates a new document with key, meta-data, and link to download from S3 bucket
* Indexes the meta-data into ElasticSearch index

## Deployment

* Create environment using devops repo: https://bitbucket.org/vk-idfood/find-a-scan-devops/src/master/
* Install python and [aws cli](https://docs.aws.amazon.com/cli/latest/userguide/awscli-install-linux.html):
```
sudo yum install zip python python-pip -y
sudo pip install awscli
```
* Enable execution: `chmod +x ./aws/update-lambda-code.sh`
* Setup aws credentials: `aws configure`
* Update function code: `./aws/update-lambda-code.sh <lambda function name from devops execution, e.g. idfood-dev-findascan-lambdafunction>`

## CI/CD

* bitbucket-pipelines.yml defines the steps
* following pipeline env variables are required:
```
AWS_ACCESS_KEY_ID
AWS_DEFAULT_REGION
AWS_SECRET_ACCESS_KEY
LAMBDA_FUNCTION_NAME
//Settings json
config
```

## Notes

* s3 event type `delete` is ignored by the lambda function