'use strict';
const assert          = require('assert');
const config          = process.env.config? 
                        JSON.parse(process.env.config):
                        require('./config/local-testing.json');
const logger          = require('../logger').create(config);
const pathParser      = require('../pathParser').create(config, logger);
const s3              = require('../s3').create(config, logger);
const bucketName      = 'idfood-dev-findascan-s3bucket';
const sourcePath      = './media/folderA/aaa.txt';
const targetPath      = 'media/folderA/aaa.txt';
const es              = require('../esClient').create(config, logger);
const s3Handler       = require('../s3Handler').create(config, logger, pathParser, s3, es);
const payload         = require('./lambda-payloads.json');

//dulication of s3.test as the s3 object is required for the s3handler testing
describe('s3', function () {
  it('uploadObject', function (done) {
    s3.uploadObject(sourcePath, bucketName, targetPath)
    .then((response) => {
      assert.equal(response.key, targetPath, 'Target path missmatch');
      done();
    })
    .catch(err => {
      logger.error(JSON.stringify(err));
      done();
    });
  });
});
         
describe('s3handler', function () {

  it('index one object', function (done) {
    s3Handler.process(payload)
    .then((response) => {
      assert.equal(response.errors, false, 'Should have no errors');
      done();
    })
    .catch(done);
  }).timeout(5000);

});