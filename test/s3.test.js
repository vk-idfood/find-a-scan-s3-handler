'use strict';
const assert          = require('assert');
const config          = process.env.config? 
                        JSON.parse(process.env.config):
                        require('./config/local-testing.json');
const logger          = require('../logger').create(config);
const s3              = require('../s3').create(config, logger);
const bucketName      = 'idfood-dev-findascan-s3bucket';
const sourcePath      = './media/folderA/aaa.txt';
const targetPath      = 'media/folderA/aaa.txt';

describe('s3', function () {

  it('uploadObject', function (done) {
    s3.uploadObject(sourcePath, bucketName, targetPath)
    .then((response) => {
      assert.equal(response.key, targetPath, 'Target path missmatch');
      done();
    })
    .catch(err => {
      logger.error(JSON.stringify(err));
      done();
    });
  });
  
  it('getObject', function (done) {
    s3.getObject(bucketName, targetPath)
    .then(response => {
      assert(response.LastModified, 'LastModified should be empty');
      done();
    })
    .catch(err => {
      logger.error(JSON.stringify(err));
      done();
    });
  });

  it('getObjectMetaData', function (done) {
    s3.getObjectMetaData(bucketName, targetPath)
    .then((response) => {
      assert(response.LastModified, 'LastModified shuold not be null');
      done();
    })
    .catch(done);
  });  

});