'use strict';
const assert                = require('assert');
const config                = process.env.config? 
                              JSON.parse(process.env.config):
                              require('./config/local-testing.json');
const logger                = require('../logger').create(config);
const pathParser            = require('../pathParser').create(config, logger);

describe('pathParser', function () {
  
  let path = 'media/folderA/aaa.txt';

  it(path, function (done) {
    pathParser.parse(path)
    .then((response) => {
      assert.equal(response.base, 'aaa.txt', 'File name should be aaa.txt');
      done();
    })
    .catch(done);
  });

});