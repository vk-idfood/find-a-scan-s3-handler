'use strict';
const config                = process.env.config? 
                              JSON.parse(process.env.config):
                              require('./config/local-testing.json');
const logger                = require('./logger').create(config);
const pathParser            = require('./pathParser').create(config, logger);
const s3                    = require('./s3').create(config, logger);
const es                    = require('./esClient').create(config, logger);
const s3Handler             = require('./s3Handler').create(config, logger, pathParser, s3, es);

exports.handler = (event, context, callback) => {
  
  logger.info(`Lambda Received Event: ${JSON.stringify(event)}`);
  
  s3Handler.process(event)
    .then((response) => {
      callback(null, `Finished processing ${event.Records.length} record(s)`);
    })
    .catch(err => {
      callback(err);
    });
};
