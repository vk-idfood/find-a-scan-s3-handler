'use strict';

const promise             = require('bluebird');
const crypto              = require('crypto');
const aws                 = require('aws-sdk');

exports.create = function (config, logger) {

  //No aws setup is required when running in a Lambda context
  if (config.aws.region) {
    aws.config.update({
      region: config.aws.region,
      accessKeyId: config.aws.username,
      secretAccessKey: config.aws.password
    });
  }
  
  const esClient = promise.promisifyAll(new (require('elasticsearch')).Client({
    hosts: config.elasticSearch.hosts,
    connectionClass: require('http-aws-es')
  }));
     
  const index = docsToIndex => {

    const action = { index:  { _index: config.elasticSearch.indexName, _type: config.elasticSearch.typeName} };
    
    const bulkBody = docsToIndex.reduce((result, doc) => {
        const id = crypto.createHash('md5').update(doc.key).digest('hex');
        doc.id = id;
        action.index._id = id
        result.push(action, doc);
        return result;
      }, []);
    
    return esClient.bulkAsync({body: bulkBody})
      .then(response => {
        if (response.errors == true) {
          return promise.reject(response);
        } else {
          return promise.resolve(response);
        }
      })
      .catch(err => {
        logger.error(JSON.stringify(err));
        return promise.reject(err);
      });    
    
  }

  return (function () {
    return {
      index: index
    };
  }());
};