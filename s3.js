'use strict';
const promise           = require('bluebird');
const awsSdk            = require('aws-sdk');
const fs                = promise.promisifyAll(require('fs'));

exports.create = function (config, logger) {

  //No aws setup is required when running in a Lambda context
  if (config.aws.region) {
    awsSdk.config.update({
      region: config.aws.region,
      accessKeyId: config.aws.username,
      secretAccessKey: config.aws.password
    });
  }
  
  const s3 = promise.promisifyAll(new awsSdk.S3());

  const handleNotFound = (key, err) => {
    if (err.cause.code === 'NotFound') {
      logger.info(JSON.stringify(err));
      return promise.reject(new NotFound({error: `Object key: ${key} was not found`}));
    } else {
      logger.error(JSON.stringify(err));
      return promise.reject(err);
    }
  };

  function NotFound(message) {
    this.message = message;
    this.name = 'NotFound';
    Error.captureStackTrace(this, NotFound);
  }

  NotFound.prototype = Object.create(Error.prototype);
  NotFound.prototype.constructor = NotFound;
  
  const getObject = (bucket, path) => {
    const readParams = {
      Bucket : bucket,
      Key: path
    };

    return s3.getObjectAsync(readParams)
      .then(s3Object => {
          return promise.resolve(s3Object);
      })
      .catch(err => {
        logger.error(JSON.stringify(err));
        return promise.reject(err);
      });
  };
  
  const getObjectMetaData = (bucket, path) => {
    const params = {
      Bucket: bucket, 
      Key: path
    };
    
    return s3.headObjectAsync(params)
      .then(data => {
        return promise.resolve(data);
      })
      .catch(err => {
        return handleNotFound(path, err);
      });
  };

  const uploadObject = (sourcePath, bucketName, targetPath) => {
    return fs.readFileAsync(sourcePath)
      .then(fileData => {
        const uploadParams = {
          Bucket: bucketName, 
          Key: targetPath, 
          Body: fileData
        };
        return s3.uploadAsync(uploadParams)
          .then(result => {
            return promise.resolve(result);
          });
      })
      .catch(err => {
        logger.error(err);
        return promise.reject(err);
      });
  };
  return (function () {
    return {
      NotFound: NotFound,
      getObject: getObject,
      getObjectMetaData: getObjectMetaData,
      uploadObject: uploadObject
    };
  }());
};