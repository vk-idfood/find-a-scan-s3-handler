'use strict';
const promise           = require('bluebird');

exports.create = function (config, logger, pathParserApi, s3Api, elasticSearchApi) {

  const prepareDoc = (objectId) => {
    
    logger.info(`Processing s3 object with key: ${objectId.key}`);
    
    return pathParserApi.parse(objectId.key)
      .then(parsedKey => {
        return s3Api.getObjectMetaData(objectId.bucket, objectId.key)
          .then(metaData => {
            
            const eTag = metaData.ETag.split('"').join('');
            const tags = parsedKey.dir.split("/").filter(segment => segment);
            tags.push(parsedKey.base);
          
            const docToIndex = {
              link: `https://s3.${objectId.region}.amazonaws.com/${objectId.bucket}/${objectId.key}`,
              key: objectId.key,
              tags: tags,
              fileName: parsedKey.base,
              extension: parsedKey.ext,
              lastModified: metaData.LastModified,
              contentType: metaData.ContentType,
              eTag: eTag
            };
          
            logger.info(`Indexing document: ${JSON.stringify(docToIndex)}`);
            return promise.resolve(docToIndex);
        })
    })
    .catch(err => {
      logger.error(`Failed to index document with key: ${objectId.key}, error: ${JSON.stringify(err)}`);
      return promise.reject(err);
    })    
  };
  
  const process = (payload) => {
    const promises = payload.Records
      //Ignore delete and other events
      .filter(r => r.eventName.includes('ObjectCreated'))
      .map(r => {
        return {
            key: r.s3.object.key,
            bucket: r.s3.bucket.name,
            region: r.awsRegion
          };
        })
      .map(objectId => prepareDoc(objectId));
    return promise.all(promises)
      .then(docsToIndex => {
        return elasticSearchApi.index(docsToIndex);
      });
  };
  
  return (function () {
    return {
      process: process
    };
  }());
};