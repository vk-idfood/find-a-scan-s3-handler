baseFileName="find-a-scan-s3handler-"
version=$(date +"%Y-%m-%d-%H-%M-%S")
export fileName=$baseFileName$version.zip
export fileNameLatest=$baseFileName'latest.zip'
zip $fileName -r ./ -x *.git*

cp ./$fileName ./$fileNameLatest

curl --user $BB_USERNAME:$BB_PASSWORD \
      -XPOST https://api.bitbucket.org/2.0/repositories/vk-idfood/find-a-scan-s3-handler/downloads \
      -F files=@$fileName -F files=@$fileNameLatest
        
 rm ./$fileName ./$fileNameLatest